#pragma once

float get_TH_temperature();
float get_TH_humidity();
void th_init();

void doPhSetup();
float getPhMeasure();
float calibrate_ph(float raw_data);

void ec_enable();
float getECMeasure();
float calibrate_ec(float pulseTime);

void co2_init();
float getCO2Measure();
float calibrate_co2(float raw_data);
float getPARMeasure_serial();
bool getCO2DigitalStatus();

float light_in_read();
float light_out_read();
bool light_in_begin();
bool light_out_begin();
